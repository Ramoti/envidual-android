package com.example.enviduallivegraph

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import com.example.enviduallivegraph.model.Graph

@Composable
fun LiveGraph(
    viewModel: EnvidualViewModel,
) {
    Column() {
        DecoratedGraph(
            graphLiveData = viewModel.level
        )
        DecoratedGraph(
            graphLiveData = viewModel.watt
        )
    }
}

@Composable
private fun DecoratedGraph(
    graphLiveData: LiveData<Graph>
) {
    val graph: Graph by graphLiveData.observeAsState(Graph())

    Card(
        shape = RoundedCornerShape(4.dp),
        elevation = 12.dp,
        modifier = Modifier
            .padding(12.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.Start
        ) {
            Graph(
                modifier = Modifier
                    .height(250.dp)
                    .fillMaxWidth()
                    .padding(12.dp),
                graph = graph
            )
            Row(modifier = Modifier.padding(12.dp)) {
                // this could be made prettier
                Text(text = graph.name)
                Spacer(modifier = Modifier.width(50.dp))
                Text(text = "Average: ${graph.average}")
                Spacer(modifier = Modifier.width(8.dp))
                Text(text = "Max: ${graph.max}")
                Spacer(modifier = Modifier.width(40.dp))
                if (graph.data.isNotEmpty())
                    Text(text = graph.data.last().toString(), fontSize = 24.sp, fontWeight = FontWeight.Bold)

            }
        }

    }
}

@Composable
private fun Graph(
    modifier: Modifier = Modifier,
    graph: Graph
) {
    Canvas(modifier = modifier) {

        // assumption: 1 entry per second over 5 min --> 300
        val maxAmountOfValues = 300

        val canvasHeight = size.height
        val canvasWidth = size.width
        val distanceBetweenDots: Float = canvasWidth / maxAmountOfValues
        var currentX = 0F

        // details
        for (i in 1..3) {
            val quarterOfHeight = canvasHeight / 4
            val y = quarterOfHeight * i
            drawLine(
                start = Offset(x = 0f, y = y),
                end = Offset(x = canvasWidth, y = y),
                color = Color.DarkGray,
                strokeWidth = 2F
            )
        }

        // plot live data
        graph.data.forEachIndexed { index, _ ->
            if (graph.data.size >= index + 2) {
                drawLine(
                    start = Offset(
                        x = currentX,
                        y = getYCoordinate(
                            range = graph.until,
                            value = graph.data[index],
                            canvasHeight = canvasHeight
                        )
                    ),
                    end = Offset(
                        x = currentX + distanceBetweenDots,
                        y = getYCoordinate(
                            range = graph.until,
                            value = graph.data[index + 1],
                            canvasHeight = canvasHeight
                        )
                    ),
                    color = Color(0xFF0277BD),
                    strokeWidth = Stroke.DefaultMiter
                )
            }
            currentX += distanceBetweenDots
        }
    }
}

private fun getYCoordinate(range: Int, value: Int, canvasHeight: Float): Float {
    val relativeToRange: Float = value.toFloat() / range.toFloat()
    return canvasHeight * (1 - relativeToRange)
}
