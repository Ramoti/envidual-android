package com.example.enviduallivegraph.model

data class Graph(
    var name: String = "",
    var data: MutableList<Int> = mutableListOf(),
    var from: Int = 0,
    var until: Int = 600,
    var newValue: Int? = null       // proxy to trigger ui update, could be anything
) {
    val max: Int
        get() = data.maxOrNull() ?: 0
    val average: Int
        get() = data.average().toInt()
}
