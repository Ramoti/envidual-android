package com.example.enviduallivegraph

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import com.example.enviduallivegraph.ui.theme.EnvidualLiveGraphTheme


class MainActivity : ComponentActivity() {
    private val viewModel: EnvidualViewModel by viewModels()

    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EnvidualLiveGraphTheme {
                Surface(color = MaterialTheme.colors.background) {
                    GraphContainer(viewModel = viewModel)
                }
            }
        }
    }
}

@Composable
fun GraphContainer(viewModel: EnvidualViewModel) {
    LiveGraph(viewModel = viewModel)
}