package com.example.enviduallivegraph

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.enviduallivegraph.model.Graph
import kotlin.concurrent.fixedRateTimer
import kotlin.random.Random

class EnvidualViewModel: ViewModel() {
    private val _level: MutableLiveData<Graph> = MutableLiveData(Graph(name = "Level", from = 0, until = 24))
    val level: LiveData<Graph> = _level

    private val _watt: MutableLiveData<Graph> = MutableLiveData(Graph(name = "Watt", from = 0, until = 600))
    val watt: LiveData<Graph> = _watt

    init {
        requestUpdates()
    }

    private fun requestUpdates() {
        fixedRateTimer(
            name = "Level",
            daemon = true,
            initialDelay = 0,
            period = 1000
        ) {
            addValues().also {
                if (!it) this.cancel()
            }
        }
    }

    /**
     * adds new entries to the graph's source list
     */
    private fun addValues(): Boolean {
        // this is not pretty but it works
        val copyLevel: Graph = _level.value?.copy()!!
        val copyWatt: Graph = _watt.value?.copy()!!

        var valueLevel = Random.nextInt(from = 0, until = 24)
        val valueWatt = Random.nextInt(from = 0, until = 600)

        if (copyLevel.data.size.mod(15) == 0) {
            copyLevel.data.add(valueLevel)
        } else {
            copyLevel.data.add(copyLevel.data.last())
        }

        copyLevel.newValue = valueLevel

        copyWatt.data.add(valueWatt)
        copyWatt.newValue = valueWatt

        _level.postValue(copyLevel)
        _watt.postValue(copyWatt)

        return copyLevel.data.size < 300
    }
}