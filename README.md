# Envidual Android Coding Challenge

  

- **MainActivity**: Einstiegspunkt der App, initiiert`EnvidualViewModel`, ruft `LiveGraph`-Komponente auf.
- **EnvidualViewModel**: Hält zwei `GraphValues` als `LiveData`. Simuliert neue Werte. Bei *Watt* komplett random, solange innerhalb 0 und 600; bei *Level* wird alle 15 Sekunden ein neuer Wert eingespeist. 
- **GraphValues**: Modelklasse, die relevante Informationen für den Graphen hält. Die Felder sind 
    ```
    name: String
    data: MutableList<Int>
    goesUntil: Int  
    newValue: Int?
    max: Int
    average: Int
    ```
 - **LiveGraph**:  Composed-Komponente(n). Observed die Daten aus dem ViewModel, stellt den Graphen sowie weitere Details dar. 
    	
        LiveGraph
    		    DecoratedGraph
    				    Graph
    				    ...
